package com.agh.cms.common.infrastructure.rest;

import com.agh.cms.common.domain.UserBasicInfo;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JwtFactoryTest {

    private static final String CORRECT_JWT = "eyJraWQiOiJ1dUJ5RmpOVlJYVGtkZ21ZaEs5TWdVZTV0aWR0aVR6bXY3MlwvbkdBSE9LRT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI1ZGI3MjUxYS0zODI5LTQ1ZTgtYjA0Mi1kNjA5ZGMxNjhjNzkiLCJjb2duaXRvOmdyb3VwcyI6WyJqYXZhIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9LaUR0bDVUYWUiLCJjb2duaXRvOnVzZXJuYW1lIjoibWFyY2VsIiwiYXVkIjoiMTYyZXZrajdlcXMwb2o4MGFkMWVwN3A5czAiLCJldmVudF9pZCI6ImYwNTM2YTNjLTA0OTQtMTFlOS1iM2NjLTI1OTM4ODhiZWVhMyIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTQ1MzM3MzM0LCJjdXN0b206bWFuYWdlciI6IlkiLCJleHAiOjE1NDUzNDA5MzQsImlhdCI6MTU0NTMzNzMzNCwiZW1haWwiOiJtYXJjZWxAZ21haWwuY29tIn0.XwULFNacmGw_lD02KiF8opB-vK0miUbGwO6AUNCKn5FZpAG4uiTn7KlY1lsxUI_RBuVfbL_7XkL-yMLU1KZEbmkNOII8OS2n3MjtOlZEuOB8iE0TVoHLUNb27BP3lVUafRfyJo3iyo1Kw-NX2TcWaWhsZrw6nitTdyy2xW3kW7nu91Epu4X99_pvPhTsXIAVlxA5kCh2JE68gFZPrS7L-VKt5xXSh6GEyfcQDIsQSncU1O0nq3BAjxdgnsi6NGlqxdcYhSh5otU6ZDIlYQ2G5EkjhNjyH2ySo75zy6pnHVtSXol9ruLsXxSvrs7bel778oGY4CfKxmroQ4mlZlRTdw";
    private static final String AUTHORIZATION_HEADER_FORMAT = "Bearer ";

    private JwtFactory jwtFactory;
    private MockHttpServletRequest request;

    @Before
    public void setUp() {
        jwtFactory = new JwtFactory();
        request = new MockHttpServletRequest();
    }

    @Test(expected = Exception.class)
    public void userJwtInfo_whenAuthorizationHeaderIsMissing_throwsException() {
        jwtFactory.userJwtInfo(request);
    }

    @Test(expected = Exception.class)
    public void userJwtInfo_whenAuthorizationHeaderIsIncorrect_throwsException() {
        request.addHeader(HttpHeaders.AUTHORIZATION, "Incorrect");

        jwtFactory.userJwtInfo(request);
    }

    @Test(expected = Exception.class)
    public void userJwtInfo_whenJwtHasInvalidFormat_throwsException() {
        request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + "Incorrect");

        jwtFactory.userJwtInfo(request);
    }

    @Test
    public void userJwtInfo_whenJwtIsCorrect_expectsCorrectUserJwtInfo() {
        request.addHeader(HttpHeaders.AUTHORIZATION, AUTHORIZATION_HEADER_FORMAT + CORRECT_JWT);

        UserBasicInfo userBasicInfo = jwtFactory.userJwtInfo(request);

        assertEquals("marcel", userBasicInfo.username());
        assertEquals("java", userBasicInfo.group());
        assertTrue(userBasicInfo.isManager());
    }
}