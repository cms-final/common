package com.agh.cms.common.config;

import com.rabbitmq.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class RabbitConfiguration {

    @Bean
    ConnectionFactory connectionFactory(@Value("${rabbit}") String rabbit) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(rabbit);
        factory.setRequestedHeartbeat(30);
        factory.setConnectionTimeout(30000);
        return factory;
    }
}
