package com.agh.cms.common.config;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

@Configuration
class FeignClientConfiguration {

    @Bean
    RequestInterceptor authorizationHeaderInterceptor(HttpServletRequest request) {
        return template -> template.header(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
    }
}
