package com.agh.cms.common.infrastructure.rest;

import com.agh.cms.common.domain.UserBasicInfo;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;

/**
 * Class responsible for extracting information from jwt. There is no need to do any validation or information checking
 * because gateway guarantees that token is valid
 */
@Configuration
class JwtFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(JwtFactory.class);

    @Bean
    @RequestScope
    UserBasicInfo userJwtInfo(HttpServletRequest request) {
        try {
            String jwt = getJwtFromRequest(request);
            DecodedJWT decodedJWT = JWT.decode(jwt);
            return getUserJwtInfoFromJwt(decodedJWT);
        } catch (Exception ex) {
            LOGGER.error("Error in JWT decoding, reason: {}", ex.getLocalizedMessage());
            throw ex;
        }
    }

    private UserBasicInfo getUserJwtInfoFromJwt(DecodedJWT decodedJWT) {
        String username = decodedJWT.getClaim("cognito:username").asString();
        String team = decodedJWT.getClaim("cognito:groups").asList(String.class).get(0);
        boolean manager = decodedJWT.getClaim("custom:manager").asString().equals("Y");
        return new UserBasicInfo(username, team, manager);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        return authorizationHeader.substring(7);
    }
}
