package com.agh.cms.common.infrastructure.service;

import com.rabbitmq.client.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class UserDeleteEventConsumerFactory {

    private static final String EXCHANGE = "users.delete";

    private final Channel channel;
    private final String queueName;

    UserDeleteEventConsumerFactory(ConnectionFactory connectionFactory) throws Exception {
        Connection connection = connectionFactory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE, "fanout");
        AMQP.Queue.DeclareOk queue = channel.queueDeclare();
        queueName = queue.getQueue();
        channel.queueBind(queueName, EXCHANGE, "");
    }

    public void createConsumer(Consumer consumer) throws IOException {
        channel.basicConsume(queueName, consumer);
    }

    public Channel getChannel() {
        return channel;
    }
}
