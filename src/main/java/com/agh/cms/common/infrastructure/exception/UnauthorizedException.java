package com.agh.cms.common.infrastructure.exception;

public final class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super("You are not allowed to perform this operation");
    }
}
