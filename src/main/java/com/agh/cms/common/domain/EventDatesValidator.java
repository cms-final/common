package com.agh.cms.common.domain;

import java.time.LocalDateTime;

public class EventDatesValidator {

    private EventDatesValidator() {
    }

    public static boolean isValid(LocalDateTime startDate, LocalDateTime endDate) {
        return endDate.isAfter(startDate);
    }
}
