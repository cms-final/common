package com.agh.cms.common.domain;

import org.springframework.util.CollectionUtils;

import java.util.Set;

public final class EventUsersGroupsValidator {

    private EventUsersGroupsValidator() {
    }

    public static boolean isValid(Set<String> users, Set<String> groups) {
        return areUsersOrGroupsNotEmpty(users, groups);
    }

    private static boolean areUsersOrGroupsNotEmpty(Set<String> users, Set<String> groups) {
        return !CollectionUtils.isEmpty(users) || !CollectionUtils.isEmpty(groups);
    }
}
