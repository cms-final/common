package com.agh.cms.common.domain.exception;

public final class RabbitException extends RuntimeException {

    public RabbitException(String message) {
        super(message);
    }
}
