package com.agh.cms.common.domain.dto.validation;

import com.agh.cms.common.domain.EventDatesValidator;
import com.agh.cms.common.domain.EventUsersGroupsValidator;
import com.agh.cms.common.domain.dto.EventCreateRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public final class EventCreateRequestValidator implements ConstraintValidator<EventCreateRequestValid, EventCreateRequest> {

    @Override
    public void initialize(EventCreateRequestValid constraintAnnotation) {
    }

    @Override
    public boolean isValid(EventCreateRequest eventCreateRequest, ConstraintValidatorContext context) {
        return EventUsersGroupsValidator.isValid(eventCreateRequest.users, eventCreateRequest.groups)
                && EventDatesValidator.isValid(eventCreateRequest.startDate, eventCreateRequest.endDate);
    }
}
