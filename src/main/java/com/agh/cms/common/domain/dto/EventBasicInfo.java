package com.agh.cms.common.domain.dto;

import com.agh.cms.common.domain.EventType;

import java.time.LocalDateTime;

public class EventBasicInfo {

    public Integer id;

    public String title;

    public EventType type;

    public LocalDateTime startDate;

    public LocalDateTime endDate;
}
