package com.agh.cms.common.domain.exception;

public final class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }
}
