package com.agh.cms.common.domain;

public enum EventType {

    TRAINING, MEETING, CONFERENCE, DELEGATION, VACATION, WORKING_DAY;
}
