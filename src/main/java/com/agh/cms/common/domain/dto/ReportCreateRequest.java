package com.agh.cms.common.domain.dto;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public final class ReportCreateRequest {

    @NotEmpty(message = "worker can't be empty")
    public String worker;

    @NotNull(message = "report type can't be null")
    public ReportType reportType;

    @NotEmpty
    public List<ReportRow> reportRows;
}
