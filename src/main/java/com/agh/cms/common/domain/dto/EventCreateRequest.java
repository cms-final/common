package com.agh.cms.common.domain.dto;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.dto.validation.EventCreateRequestValid;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Validated
@EventCreateRequestValid
public class EventCreateRequest {

    @NotEmpty(message = "title can't be empty")
    public String title;

    public String description;

    @NotNull(message = "type can't be null")
    public EventType type;

    @FutureOrPresent
    @NotNull(message = "startDate date can't be null")
    public LocalDateTime startDate;

    @FutureOrPresent
    @NotNull(message = "endDate date can't be null")
    public LocalDateTime endDate;

    @NotNull(message = "users date can't be null")
    public Set<String> users;

    @NotNull(message = "groups date can't be null")
    public Set<String> groups;
}
