package com.agh.cms.common.domain;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Validated
public class UserBasicInfo {

    @NotEmpty(message = "username can't be empty")
    private final String username;

    @NotEmpty(message = "group can't be empty")
    private final String group;

    private final boolean manager;

    public UserBasicInfo(@NotEmpty(message = "username can't be null") String username,
                         @NotEmpty(message = "group can't be empty") String group, boolean manager) {
        this.username = username;
        this.group = group;
        this.manager = manager;
    }

    public String username() {
        return username;
    }

    public boolean isManager() {
        return manager;
    }

    public String group() {
        return group;
    }
}
