package com.agh.cms.common.domain.dto;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;

@Validated
public final class ReportRow {

    @NotEmpty(message = "from can't be empty")
    public String from;

    @NotEmpty(message = "to can't be empty")
    public String to;
}