package com.agh.cms.common.domain.dto;

public enum ReportType {

    VACATIONS("Vacations"), WORKING_DAYS("Working days");

    private final String name;

    ReportType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}